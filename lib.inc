%define EXIT 60
%define SYSCALL_WRITE 1
%define STDOUT 1
%define TAB 0x9
%define NEW_LINE 0xA
%define SPACE 0x20


section .text
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, EXIT
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
	    cmp byte [rax + rdi], 0 
	    je .end ; if ZF = 1 -> .end
        inc rax
    	jmp .loop
    .end: 
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    ;xor rax, rax
    push rdi
    call string_length ; в rax лежит кол-во символов
    pop rsi
    mov rdx, rax; сохранить в rdx длину строки 
    mov rax, SYSCALL_WRITE
    mov rdi, STDOUT
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    ;xor rax, rax
    push rdi
    mov rsi, rsp
    mov rax, SYSCALL_WRITE
    mov rdi, STDOUT
    mov rdx, 1; кол-во байт для записи
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    ;xor rax, rax
    mov rdi, NEW_LINE
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    ;xor rax, rax
    mov rax, rdi
    mov r8, rsp; сохраняем адрес вершины аппаратного стека
    mov r9, 10; делитель
    
    dec rsp; в стек положили 0 - терминатор
    mov [rsp], byte 0 
    
    .loop:
	    xor rdx, rdx
        dec rsp
	    div r9
	    add rdx, '0'
	    mov [rsp], dl
	    test rax, rax
	    jnz .loop
    
    .end:
	    mov rdi, rsp
        push r8
	    call print_string
        pop rsp
        ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    ;xor rax, rax
    cmp rdi, 0
    jns .print

    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi

    .print:
        jmp print_uint


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    xor rcx, rcx; счетчик символа


    .loop:
        mov r8b, byte[rdi + rcx]
        mov r9b, byte[rsi + rcx]
        cmp r8b, r9b
        jne .end
        cmp r8b, 0
        je .good
        inc rcx
        jmp .loop

    .end:
        ret

    .good:
        inc rax
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax; номер системеного вызова read
    mov rdi, 0; дескриптор stdin
    push 0
    mov rdx, 1
    mov rsi, rsp;куда мы будем записывать значение
    syscall
    pop rax
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    ;rdi - адрес начала буфера, rsi - размер буфера
    xor rcx, rcx; счетчик символов

    .loop:
        push rsi
        push rdi
        push rcx
        call read_char
        pop rcx
        pop rdi
        pop rsi

        cmp al, SPACE
        je .skip
        cmp al, TAB
        je .skip
        cmp al, NEW_LINE
        je .skip

        cmp rax, 0
        jz .end

        cmp rsi, rcx; проверка не выходим ли за буфер
        je .exception

        mov [rdi + rcx], al
        inc rcx

        jmp .loop

    .skip: 
        test rcx, rcx
        jz .loop

    .end:
        xor rax, rax
        mov [rdi+rcx], al
        mov rax, rdi
        mov rdx, rcx
        ret

    .exception:
        xor rax, rax
        ret



; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax; число
    xor rdx, rdx; длина 
    mov r8, 10
    xor rcx, rcx

    .loop: 
        mov dl, byte[rdi+rcx]
        cmp rdx, '9'
        jg .end 
        cmp rdx, '0'
        jl .end
        sub rdx, '0'
        push rdx
        mul r8
        pop rdx
        add rax, rdx
        inc rcx
        jmp .loop

    .end:
        mov rdx, rcx
        ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax; число
    xor rdx, rdx; длина(+знак)
    
    cmp byte [rdi], 0x2D
    je .negative
    jmp parse_uint

    .negative:
        inc rdi
        call parse_uint
        neg rax
        inc rdx
        ret


; Принимает указатель на строку(rdi), указатель на буфер(rsi) и длину буфера(rdx)
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    xor rcx, rcx;

    .loop:
        cmp rdx, rcx
        je .endBuffer
        mov rax, [rdi + rcx]
        cmp rax, 0 
        je .end
        mov [rsi + rcx], rax
        inc rcx
        jmp .loop
        
    .endBuffer:
        xor rax, rax
        ret
    
    .end: 
        mov rax, rcx
        ret
